﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LcdInitialScriptEditor
{
    public class Config
    {
        private static volatile Config _Instance;
        private static object _SyncRoot = new Object();
        private object _Lock = new object();

        #region Instance

        public static Config Instance
        {
            get
            {
                if (_Instance == null)
                {
                    lock (_SyncRoot)
                    {
                        if (_Instance == null)
                            _Instance = new Config();
                    }
                }

                return _Instance;
            }
        }
        #endregion Instance


        #region ScriptFilePath
        private string _ScriptFilePath;

        public string ScriptFilePath
        {
            get { lock (_Lock) { return _ScriptFilePath; } }
            set { lock (_Lock) { _ScriptFilePath = value; } }
        }

        #endregion
    }
}
