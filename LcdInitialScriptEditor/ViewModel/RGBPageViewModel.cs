﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LcdInitialScriptEditor.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace LcdInitialScriptEditor.ViewModel
{
    public class RGBPageViewModel : ViewModelBase
    {
        private IDialogService _DialogService;
        public RGBPageViewModel(IDialogService dialogService)
        {
            this._DialogService = dialogService;
        }

        private ChipType _ChipType;
        public ChipType ChipType 
        {
            get { return _ChipType; }
            set
            {
                if (_ChipType == value) return;
                _ChipType = value;
                RaisePropertyChanged(() => ChipType);
            }
        }


        public ICommand CreateScriptCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    // Do Action
                    ChipType ct = this.ChipType;
                    this._DialogService.ShowMessageBox("Success");
                });
            }
        }
    }
}
