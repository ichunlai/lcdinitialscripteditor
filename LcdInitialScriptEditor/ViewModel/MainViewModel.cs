using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LcdInitialScriptEditor.Services;
using System.Windows.Input;

namespace LcdInitialScriptEditor.ViewModel
{
    public enum ChipType
    {
        IT9850,
        IT970
    }

    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private IDialogService _DialogService;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDialogService dialogService)
        {
            this._DialogService = dialogService;
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
        }

        public ICommand LoadScriptCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    // Do Action
                    _DialogService.ShowOpenFileDialog(
                        "All files (*.*)|*.*", // "JPEG files (*.jpg)|*.jpg|All files (*.*)|*.*",
                        null,
                        (PickedFilePath) => { Config.Instance.ScriptFilePath = PickedFilePath; });
                });
            }
        }
    }
}