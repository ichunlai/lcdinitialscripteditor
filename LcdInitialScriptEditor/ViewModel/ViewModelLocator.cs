/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:LcdInitialScriptEditor"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using LcdInitialScriptEditor.Services;

namespace LcdInitialScriptEditor.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}
            SimpleIoc.Default.Register<IDialogService, DialogService>();

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<RGBPageViewModel>();
            SimpleIoc.Default.Register<CPUPageViewModel>();
            SimpleIoc.Default.Register<LVDSPageViewModel>();
            SimpleIoc.Default.Register<MIPIPageViewModel>();
            SimpleIoc.Default.Register<LogPageViewModel>();
            SimpleIoc.Default.Register<BatchPageViewModel>();
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public RGBPageViewModel RGBPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<RGBPageViewModel>();
            }
        }

        public CPUPageViewModel CPUPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CPUPageViewModel>();
            }
        }

        public LVDSPageViewModel LVDSPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LVDSPageViewModel>();
            }
        }

        public MIPIPageViewModel MIPIPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MIPIPageViewModel>();
            }
        }

        public LogPageViewModel LogPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LogPageViewModel>();
            }
        }

        public BatchPageViewModel BatchPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<BatchPageViewModel>();
            }
        }
        
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}