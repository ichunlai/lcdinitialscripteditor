﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace LcdInitialScriptEditor.Services
{
    public class DialogService : IDialogService
    {
        void IDialogService.ShowOpenFileDialog(string filter, string defaultFileName, Action<string> afterSelectedCallback)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (!string.IsNullOrWhiteSpace(filter))
                dialog.Filter = filter;
            if (!string.IsNullOrWhiteSpace(defaultFileName))
                dialog.FileName = defaultFileName;

            bool? result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                if (afterSelectedCallback != null)
                    afterSelectedCallback.Invoke(dialog.FileName);
            }
        }

        void IDialogService.ShowSaveFileDialog(string filter, string defaultFileName, Action<string> afterSelectedCallback)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            if (!string.IsNullOrWhiteSpace(filter))
                dialog.Filter = filter;
            if (!string.IsNullOrWhiteSpace(defaultFileName))
                dialog.FileName = defaultFileName;

            bool? result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                if (afterSelectedCallback != null)
                    afterSelectedCallback.Invoke(dialog.FileName);
            }
        }

        System.Windows.MessageBoxResult IDialogService.ShowMessageBox(string messageBoxText)
        {
            return MessageBox.Show(messageBoxText);
        }

        System.Windows.MessageBoxResult IDialogService.ShowMessageBox(string messageBoxText, string caption, System.Windows.MessageBoxButton button, System.Windows.MessageBoxImage icon, System.Windows.MessageBoxResult defaultResult, System.Windows.MessageBoxOptions options)
        {
            return MessageBox.Show(messageBoxText, caption, button, icon, defaultResult, options);
        }

        System.Windows.MessageBoxResult IDialogService.ShowMessageBox(System.Windows.Window owner, string messageBoxText)
        {
            return MessageBox.Show(owner, messageBoxText);
        }

        System.Windows.MessageBoxResult IDialogService.ShowMessageBox(System.Windows.Window owner, string messageBoxText, string caption, System.Windows.MessageBoxButton button, System.Windows.MessageBoxImage icon, System.Windows.MessageBoxResult defaultResult, System.Windows.MessageBoxOptions options)
        {
            return MessageBox.Show(owner, messageBoxText, caption, button, icon, defaultResult, options);
        }
    }
}
