﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace LcdInitialScriptEditor.Services
{
    public interface IDialogService
    {
        /// <summary>
        /// Diaplay OpenFileDialog
        /// </summary>
        /// <param name="Filter">ex. "XML files (*.xml)|*.xml|All files (*.*)|*.*";</param>
        /// <param name="DefaultFileName">ex. "addressbook.xml"</param>
        void ShowOpenFileDialog(string filter, string defaultFileName, Action<string> afterSelectedCallback = null);

        void ShowSaveFileDialog(string filter, string defaultFileName, Action<string> afterSelectedCallback = null);
        MessageBoxResult ShowMessageBox(string messageBoxText);

        MessageBoxResult ShowMessageBox(
            string messageBoxText,
            string caption,
            MessageBoxButton button = MessageBoxButton.OK,
            MessageBoxImage icon = MessageBoxImage.None,
            MessageBoxResult defaultResult = 0,
            MessageBoxOptions options = 0);

        MessageBoxResult ShowMessageBox(
           Window owner,
           string messageBoxText);

        MessageBoxResult ShowMessageBox(
             Window owner,
             string messageBoxText,
             string caption,
             MessageBoxButton button = MessageBoxButton.OK,
             MessageBoxImage icon = MessageBoxImage.None,
             MessageBoxResult defaultResult = 0,
             MessageBoxOptions options = 0);
    }
}
